import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthorizationService} from '../../../core/services/login/authorization.service';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BaseComponent implements OnInit {

  constructor(private authorizationService: AuthorizationService) {
  }

  ngOnInit(): void {
  }

  logout() {
    this.authorizationService.logout();
  }
}
