import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthorizationService} from '../../../../core/services/login/authorization.service';
import {LoginResource} from './resource/login-resource';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-auth',
    templateUrl: './login.component.html',
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    loginResource: LoginResource = {
        access_token: '',
        token_type: '',
        refresh_token: '',
        expires_in: 0,
        scope: ''
    }

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private loginService: AuthorizationService,
        private toastrService: ToastrService) {

        this.loginForm = this.fb.group({})

    }

    ngOnInit(): void {
        this.initLoginForm();
    }

    initLoginForm() {
        this.loginForm = this.fb.group({
            username: [null, Validators.compose([
                Validators.required,
                Validators.email,
                Validators.minLength(3),
                Validators.maxLength(320)
            ])
            ],
            password: [null, Validators.compose([
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(100)
            ])
            ],
            mfaCode: [null, Validators.compose([
                Validators.minLength(6),
                Validators.maxLength(19)
            ])
            ],

        });
    }

    submit() {
        this.loginService.login(this.loginForm.value.username, this.loginForm.value.password).subscribe(res => {
            if (res.status === 200) {
                this.loginResource = res.body;
                localStorage.setItem(btoa('access_token'), this.loginResource.access_token);
                this.router.navigateByUrl('/homepage');
            } else if (res.status === 400) {
                this.toastrService.info(res.error.error_description)
            }
        })
    }
}
