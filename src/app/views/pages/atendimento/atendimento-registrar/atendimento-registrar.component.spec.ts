import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AtendimentoRegistrarComponent} from './atendimento-registrar.component';

describe('AtendimentoRegistrarComponent', () => {
  let component: AtendimentoRegistrarComponent;
  let fixture: ComponentFixture<AtendimentoRegistrarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AtendimentoRegistrarComponent]
    })
        .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtendimentoRegistrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
