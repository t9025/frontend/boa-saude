import {Component, Injectable, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AtendimentoService} from '../../../../core/services/atendimento/atendimento.service';
import {RetornoAtendimentoDTO} from '../resource/retorno-atendimento-dto';
import {AtualizarAtendimentoDTO} from '../resource/atualizar-atendimento-dto';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {ExameDTO} from '../resource/exame-dto';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-atendimento-registrar',
    templateUrl: './atendimento-registrar.component.html',
    styleUrls: ['./atendimento-registrar.component.css']
})
export class AtendimentoRegistrarComponent implements OnInit {

    registrarAtendimentoForm: FormGroup;
    retornoAtendimentoDTO: RetornoAtendimentoDTO;
    exames: Array<ExameDTO>
    dataSource: MatTableDataSource<ExameDTO>;
    displayedColumns = ['prestador', 'associado', 'conveniado', 'horario', 'acao'];
    length = 100;
    pageSize = 10;
    pageSizeOptions: number[] = [1, 2, 3, 5, 10, 25, 100];
    pageEvent: PageEvent;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    constructor(private route: ActivatedRoute,
                private fb: FormBuilder,
                private atendimentoService: AtendimentoService,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private toastrService: ToastrService) {
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe(({routeResolver}) => {
            this.retornoAtendimentoDTO = routeResolver;
        })
        this.dataSource = new MatTableDataSource();
        this.exames = new Array<ExameDTO>()
        this.criarFormGroup();
    }


    criarFormGroup() {
        this.registrarAtendimentoForm = this.fb.group({
            prestador: [{value: this.retornoAtendimentoDTO.prestador, disabled: true}],
            associado: [{value: this.retornoAtendimentoDTO.associado, disabled: true}],
            conveniado: [{value: this.retornoAtendimentoDTO.conveniado, disabled: true}],
            dataAtendimento: [{value: this.retornoAtendimentoDTO.horario, disabled: true}],
            confirmado: [{value: this.retornoAtendimentoDTO.confirmado ? 'Sim' : 'Não', disabled: true}],
            observacao: [null, [Validators.maxLength(1000), Validators.required]],
            prestadorExame: [null, [Validators.required]],
            associadoExame: [{value: this.retornoAtendimentoDTO.associado, disabled: true}],
            conveniadoExame: [null, [Validators.required]],
            dataAtendimentoExame: [null, [Validators.required]],
            tipoExame: [null, [Validators.required]],
        });
    }

    salvar() {
        /** check form */
        const controls = this.registrarAtendimentoForm.controls;
        if (this.registrarAtendimentoForm.invalid) {
            Object.keys(controls).forEach(controlName =>
                controls[controlName].markAsTouched()
            );
            return;
        }

        let atualizarAtendimentoDTO = new AtualizarAtendimentoDTO()
        atualizarAtendimentoDTO.prestador = this.retornoAtendimentoDTO.prestador;
        atualizarAtendimentoDTO.conveniado = this.retornoAtendimentoDTO.conveniado;
        atualizarAtendimentoDTO.horario = this.retornoAtendimentoDTO.horario;
        atualizarAtendimentoDTO.observacoes = this.registrarAtendimentoForm.value.observacao;
        atualizarAtendimentoDTO.confirmado = this.retornoAtendimentoDTO.confirmado;

        if (this.dataSource.data.length > 0) {
            atualizarAtendimentoDTO.exames = new Array<ExameDTO>()
            this.dataSource.data.forEach(exame => atualizarAtendimentoDTO.exames.push(exame))
        }

        this.atendimentoService.atualizarAtendimento(atualizarAtendimentoDTO, this.retornoAtendimentoDTO.id).subscribe(res => {
            if (res.status === 200) {
                this.toastrService.info('Atendimento realizado com sucesso')
                this.router.navigate(['/atendimento/pesquisa']);
            } else if (res.status === 400) {
                this.toastrService.warning(res.error.message)
            }
        }, () => {
            this.toastrService.error('Falha ao realizar atendimento')
        });
    }

    adicionarExame() {
        const controls = this.registrarAtendimentoForm.controls;
        if (this.registrarAtendimentoForm.invalid) {
            Object.keys(controls).forEach(controlName =>
                controls[controlName].markAsTouched()
            );
            return;
        }

        let examesDTO = new ExameDTO();
        examesDTO.associado = this.retornoAtendimentoDTO.associado;
        examesDTO.prestador = this.registrarAtendimentoForm.value.prestadorExame;
        examesDTO.conveniado = this.registrarAtendimentoForm.value.conveniadoExame;
        examesDTO.horario = this.registrarAtendimentoForm.value.dataAtendimentoExame;
        examesDTO.tipo = this.registrarAtendimentoForm.value.tipoExame;
        examesDTO.confirmado = false;

        this.exames.push(examesDTO)
        this.dataSource = new MatTableDataSource<ExameDTO>(this.exames)
    }

    removeExame(index: number, exameDTO: ExameDTO) {
        if (confirm('Atenção! Você deseja remover o exame ' + exameDTO.tipo + '?')) {
            this.exames.splice(index, 1)
            this.dataSource = new MatTableDataSource<ExameDTO>(this.exames)
        }
    }

    isControlHasError(controlName: string, validationType: string): boolean {
        const control = this.registrarAtendimentoForm.controls[controlName];
        if (!control) {
            return false;
        }

        return control.hasError(validationType) &&
            (control.dirty || control.touched);
    }
}

@Injectable()
export class AtendimentoRegistrarComponentResolver implements Resolve<any> {

    constructor(private atendimentoService: AtendimentoService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.atendimentoService.obterAtendimento(route.params['id']);
    }
}
