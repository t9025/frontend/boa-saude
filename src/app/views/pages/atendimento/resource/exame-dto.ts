export class ExameDTO {

    associado: string;
    prestador: string;
    conveniado: string;
    horario: string;
    tipo: string;
    confirmado: boolean;
}
