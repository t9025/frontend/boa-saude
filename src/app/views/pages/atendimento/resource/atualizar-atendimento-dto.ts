import {ExameDTO} from './exame-dto';

export class AtualizarAtendimentoDTO {

    prestador: string;
    conveniado: string;
    horario: string;
    observacoes: string;
    confirmado: boolean;

    exames: Array<ExameDTO>
}
