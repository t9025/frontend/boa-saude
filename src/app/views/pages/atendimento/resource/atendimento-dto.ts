export class AtendimentoDTO {

    prestador: string;
    associado: string;
    conveniado: string;
    horario: string;
}
