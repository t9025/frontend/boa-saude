export class RetornoAtendimentoDTO {
    id: number;
    prestador: string;
    associado: string;
    conveniado: string;
    horario: string;
    observacoes: string;
    confirmado: boolean;
}
