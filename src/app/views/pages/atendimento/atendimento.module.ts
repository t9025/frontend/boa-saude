import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AtendimentoPesquisaComponent} from './atendimento-pesquisa/atendimento-pesquisa.component';
import {AtendimentoCadastroComponent} from './atendimento-cadastro/atendimento-cadastro.component';
import {
    AtendimentoRegistrarComponent,
    AtendimentoRegistrarComponentResolver
} from './atendimento-registrar/atendimento-registrar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {RouterModule} from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import {BaseAtendimentoComponent} from './base-atendimento.component';
import {AuthGuard} from '../../../core/services/auth-guard/auth-guard';
import {AuthGuardChild} from '../../../core/services/auth-guard/auth-guard-child';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatSortModule} from '@angular/material/sort';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgxMaskModule} from 'ngx-mask';
import {MatPaginatorModule} from '@angular/material/paginator';


@NgModule({
    declarations: [
        AtendimentoPesquisaComponent,
        AtendimentoCadastroComponent,
        AtendimentoRegistrarComponent,
        BaseAtendimentoComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatCardModule,
        RouterModule.forChild([
            {
                path: '',
                component: BaseAtendimentoComponent,
                canActivate: [AuthGuard],
                canActivateChild: [AuthGuardChild],
                children: [
                    {
                        path: 'atendimento',
                        redirectTo: 'pesquisa',
                        pathMatch: 'full'
                    },
                    {
                        path: 'pesquisa',
                        component: AtendimentoPesquisaComponent,
                    },
                    {
                        path: 'cadastro',
                        component: AtendimentoCadastroComponent
                    },
                    {
                        path: 'registrar/:id',
                        component: AtendimentoRegistrarComponent,
                        resolve: {
                            routeResolver: AtendimentoRegistrarComponentResolver
                        }
                    },

                ]
            }
        ]),
        MatTabsModule,
        MatTableModule,
        MatIconModule,
        MatSortModule,
        MatTooltipModule,
        NgxMaskModule.forRoot(),
        MatPaginatorModule,
    ],
    providers: [
        AtendimentoRegistrarComponentResolver
    ],
    exports: [
        RouterModule
    ]
})
export class AtendimentoModule {
}
