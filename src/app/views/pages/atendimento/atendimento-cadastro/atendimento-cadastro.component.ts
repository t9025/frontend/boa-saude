import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AtendimentoService} from '../../../../core/services/atendimento/atendimento.service';
import {AtendimentoDTO} from '../resource/atendimento-dto';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-atendimento-cadastro',
    templateUrl: './atendimento-cadastro.component.html',
    styleUrls: ['./atendimento-cadastro.component.css']
})
export class AtendimentoCadastroComponent implements OnInit {

    cadastroAtendimentoForm: FormGroup;

    constructor(private fb: FormBuilder,
                private atendimentoService: AtendimentoService,
                private router: Router,
                private toastrService: ToastrService) {
    }

    ngOnInit(): void {
        this.criarFormGroup();
    }


    criarFormGroup() {
        this.cadastroAtendimentoForm = this.fb.group({
            prestador: [null, [Validators.required]],
            associado: [null, [Validators.required]],
            conveniado: [null, [Validators.required]],
            dataAtendimento: [null, [Validators.required]]
        });
    }

    salvar() {
        const controls = this.cadastroAtendimentoForm.controls;
        /** check form */
        if (this.cadastroAtendimentoForm.invalid) {
            Object.keys(controls).forEach(controlName =>
                controls[controlName].markAsTouched()
            );
            return;
        }

        let atendimentoDTO = new AtendimentoDTO();
        atendimentoDTO.prestador = this.cadastroAtendimentoForm.value.prestador;
        atendimentoDTO.associado = this.cadastroAtendimentoForm.value.associado;
        atendimentoDTO.conveniado = this.cadastroAtendimentoForm.value.conveniado;
        atendimentoDTO.horario = this.cadastroAtendimentoForm.value.dataAtendimento;

        this.atendimentoService.salvar(atendimentoDTO).subscribe(res => {
            if (res.status === 201) {
                this.toastrService.info('Atendimento cadastrado com sucesso');
                this.limpar();
                this.router.navigate(['/atendimento/pesquisa']);
            } else if (res.status === 400) {
                this.toastrService.warning(res.error.message);
            } else if (res.status === 500) {
                this.toastrService.error('Falha ao cadastrar atendimento');
            }
        });
    }

    limpar() {
        this.cadastroAtendimentoForm.reset();
    }

    isControlHasError(controlName: string, validationType: string): boolean {
        const control = this.cadastroAtendimentoForm.controls[controlName];
        if (!control) {
            return false;
        }

        return control.hasError(validationType) &&
            (control.dirty || control.touched);
    }
}
