import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AtendimentoPesquisaComponent} from './atendimento-pesquisa.component';

describe('AtendimentoPesquisaComponent', () => {
  let component: AtendimentoPesquisaComponent;
  let fixture: ComponentFixture<AtendimentoPesquisaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AtendimentoPesquisaComponent]
    })
        .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtendimentoPesquisaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
