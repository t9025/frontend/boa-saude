import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AtendimentoService} from '../../../../core/services/atendimento/atendimento.service';
import {RetornoAtendimentoDTO} from '../resource/retorno-atendimento-dto';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {AtualizarAtendimentoDTO} from '../resource/atualizar-atendimento-dto';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-atendimento-pesquisa',
    templateUrl: './atendimento-pesquisa.component.html',
    styleUrls: ['./atendimento-pesquisa.component.css']
})
export class AtendimentoPesquisaComponent implements OnInit, AfterViewInit {

    pesquisarAtendimentoForm: FormGroup;
    dataSource: MatTableDataSource<RetornoAtendimentoDTO>;
    displayedColumns = ['prestador', 'associado', 'conveniado', 'horario', 'observacoes', 'confirmado', 'acao'];
    length = 100;
    pageSize = 10;
    pageSizeOptions: number[] = [1, 2, 3, 5, 10, 25, 100];
    pageEvent: PageEvent;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    constructor(private fb: FormBuilder,
                private atendimentoService: AtendimentoService,
                private router: Router,
                private toastrService: ToastrService) {
    }

    ngOnInit(): void {
        this.dataSource = new MatTableDataSource();
        this.criarFormGroup();
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    criarFormGroup() {
        this.pesquisarAtendimentoForm = this.fb.group({
            prestador: [null],
            associado: [null],
            conveniado: [null],
            dataAtendimento: [null],
            observacoes: [null],
            confirmado: [null]
        });
    }

    pesquisar() {

        const prestador = this.pesquisarAtendimentoForm.value.prestador;
        const associado = this.pesquisarAtendimentoForm.value.associado;
        const conveniado = this.pesquisarAtendimentoForm.value.conveniado;
        const dataAtendimento = this.pesquisarAtendimentoForm.value.dataAtendimento;
        const confirmado = this.pesquisarAtendimentoForm.value.confirmado;

        this.atendimentoService.pesquisar(associado, prestador, conveniado, dataAtendimento, confirmado).subscribe((data: Array<RetornoAtendimentoDTO>) => {
            this.dataSource.data = data;
        })
    }

    limpar() {
        this.pesquisarAtendimentoForm.reset();
    }

    openDialogConfirmacao(retornoAtendimentoDTO: RetornoAtendimentoDTO): void {
        if (confirm('Atenção! Gostaria de confirmar o atendimento do sr(a) ' + retornoAtendimentoDTO.associado + '?')) {
            this.confirmarAtendimento(retornoAtendimentoDTO);
        }
    }

    confirmarAtendimento(retornoAtendimentoDTO: RetornoAtendimentoDTO) {
        let atualizarAtendimentoDTO = new AtualizarAtendimentoDTO()
        atualizarAtendimentoDTO.prestador = retornoAtendimentoDTO.prestador;
        atualizarAtendimentoDTO.conveniado = retornoAtendimentoDTO.conveniado;
        atualizarAtendimentoDTO.horario = retornoAtendimentoDTO.horario;
        atualizarAtendimentoDTO.observacoes = retornoAtendimentoDTO.observacoes;
        atualizarAtendimentoDTO.confirmado = true;

        this.atendimentoService.atualizarAtendimento(atualizarAtendimentoDTO, retornoAtendimentoDTO.id).subscribe(res => {
            if (res.status === 200) {
                this.toastrService.info('Atendimento confirmado com sucesso')
                this.pesquisar()
            } else if (res.status === 400) {
                this.toastrService.warning(res.error.message)
            }
        }, () => {
            this.toastrService.error('Falha ao editar usuário')
        });
    }

    openDialogIniciarAtendimento(retornoAtendimentoDTO: RetornoAtendimentoDTO) {
        if (confirm('Atenção! Gostaria de iniciar o atendimento do sr(a) ' + retornoAtendimentoDTO.associado + '?')) {
            this.router.navigate(['atendimento/registrar', retornoAtendimentoDTO.id]);
        }
    }
}
