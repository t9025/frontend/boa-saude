import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {RouterModule} from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import {HomepageComponent} from './homepage.component';
import {AuthGuard} from '../../../core/services/auth-guard/auth-guard';


@NgModule({
  declarations: [
    HomepageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomepageComponent,
        canActivate: [AuthGuard],
      }
    ]),
    MatCardModule,
  ]
})
export class HomepageModule {
}
