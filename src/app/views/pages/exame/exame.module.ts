import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExamePesquisaComponent} from './exame-pesquisa/exame-pesquisa.component';
import {BaseExameComponent} from './base-exame.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatCardModule} from '@angular/material/card';
import {RouterModule} from '@angular/router';
import {BaseAtendimentoComponent} from '../atendimento/base-atendimento.component';
import {AuthGuard} from '../../../core/services/auth-guard/auth-guard';
import {AuthGuardChild} from '../../../core/services/auth-guard/auth-guard-child';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatSortModule} from '@angular/material/sort';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgxMaskModule} from 'ngx-mask';
import {MatPaginatorModule} from '@angular/material/paginator';


@NgModule({
  declarations: [
    ExamePesquisaComponent,
    BaseExameComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatCardModule,
    RouterModule.forChild([
      {
        path: '',
        component: BaseAtendimentoComponent,
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuardChild],
        children: [
          {
            path: 'exame',
            redirectTo: 'pesquisa',
            pathMatch: 'full'
          },
          {
            path: 'pesquisa',
            component: ExamePesquisaComponent,
          }
        ]
      }
    ]),
    MatTabsModule,
    MatTableModule,
    MatIconModule,
    MatSortModule,
    MatTooltipModule,
    NgxMaskModule.forRoot(),
    MatPaginatorModule,
  ]
})
export class ExameModule {
}
