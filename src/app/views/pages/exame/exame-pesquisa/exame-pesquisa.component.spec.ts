import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ExamePesquisaComponent} from './exame-pesquisa.component';

describe('ExamePesquisaComponent', () => {
  let component: ExamePesquisaComponent;
  let fixture: ComponentFixture<ExamePesquisaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExamePesquisaComponent]
    })
        .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamePesquisaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
