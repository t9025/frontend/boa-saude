import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {ExameService} from '../../../../core/services/exame/exame.service';
import {ExameDTO} from '../resource/exame-dto';

@Component({
    selector: 'app-exame-pesquisa',
    templateUrl: './exame-pesquisa.component.html',
    styleUrls: ['./exame-pesquisa.component.css']
})
export class ExamePesquisaComponent implements OnInit {

    pesquisarExameForm: FormGroup;
    dataSource: MatTableDataSource<ExameDTO>;
    displayedColumns = ['prestador', 'associado', 'conveniado', 'horario', 'confirmado', 'tipo', 'acao'];
    length = 100;
    pageSize = 10;
    pageSizeOptions: number[] = [1, 2, 3, 5, 10, 25, 100];
    pageEvent: PageEvent;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;


    constructor(private fb: FormBuilder,
                private exameService: ExameService,
                private router: Router,
                private toastrService: ToastrService) {
    }

    ngOnInit(): void {
        this.dataSource = new MatTableDataSource();
        this.criarFormGroup();
    }

    ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    criarFormGroup() {
        this.pesquisarExameForm = this.fb.group({
            prestador: [null],
            associado: [null],
            conveniado: [null],
            dataAtendimento: [null],
            observacoes: [null],
            confirmado: [null],
            tipo: [null]
        });
    }

    pesquisar() {

        const prestador = this.pesquisarExameForm.value.prestador;
        const associado = this.pesquisarExameForm.value.associado;
        const conveniado = this.pesquisarExameForm.value.conveniado;
        const dataAtendimento = this.pesquisarExameForm.value.dataAtendimento;
        const confirmado = this.pesquisarExameForm.value.confirmado;
        const tipo = this.pesquisarExameForm.value.tipo;

        this.exameService.pesquisar(associado, prestador, conveniado, dataAtendimento, confirmado, tipo).subscribe((data: Array<ExameDTO>) => {
            this.dataSource.data = data;
        })
    }

    limpar() {
        this.pesquisarExameForm.reset();
    }

    openDialogConfirmacao(exameDTO: ExameDTO): void {
        if (confirm('Atenção! Gostaria de confirmar o exame do sr(a) ' + exameDTO.associado + '?')) {
            this.aprovarExame(exameDTO);
        }
    }


    aprovarExame(exameDTO: ExameDTO) {
        this.exameService.aprovarExame('AUTORIZADO', exameDTO.id).subscribe(res => {
            if (res.status === 200) {
                this.toastrService.info('Exame autorizado com sucesso')
                this.pesquisar()
            } else if (res.status === 400) {
                this.toastrService.warning(res.error.message)
            } else {
                this.toastrService.error('Falha ao autorizar o exame')
            }
        });
    }

    removerAutorizacaoExame(ExameDTO: ExameDTO) {
        this.exameService.aprovarExame('NAO_AUTORIZADO', ExameDTO.id).subscribe(res => {
            if (res.status === 200) {
                this.toastrService.info('Exame não autorizado com sucesso')
                this.pesquisar()
            } else if (res.status === 400) {
                this.toastrService.warning(res.error.message)
            }
        }, () => {
            this.toastrService.error('Falha ao autorizar o exame')
        });
    }

    openDialogRemoverConfirmacao(ExameDTO: ExameDTO) {
        if (confirm('Atenção! Gostaria de desautorizar o exame do sr(a) ' + ExameDTO.associado + '?')) {
            this.removerAutorizacaoExame(ExameDTO);
        }
    }

}
