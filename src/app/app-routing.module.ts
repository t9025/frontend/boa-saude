import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AuthModule} from './views/pages/auth/auth.module';
import {HomepageModule} from './views/pages/homepage/homepage.module';
import {AuthGuard} from './core/services/auth-guard/auth-guard';
import {AtendimentoModule} from './views/pages/atendimento/atendimento.module';
import {BaseComponent} from './views/theme/base/base.component';
import {ExameModule} from './views/pages/exame/exame.module';

const routes: Routes = [
    {
        path: '', loadChildren: () => AuthModule,
    },
    {
        path: '',
        component: BaseComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'homepage',
                loadChildren: () => HomepageModule
            },
            {
                path: 'atendimento',
                loadChildren: () => AtendimentoModule
            },
            {
                path: 'exame',
                loadChildren: () => ExameModule
            },

            {path: '', redirectTo: 'homepage', pathMatch: 'full'},
            {path: '**', redirectTo: 'error/not-found', pathMatch: 'full'}
        ]
    },
];
export const routing = RouterModule.forRoot(routes)

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forRoot(routes, {useHash: true, relativeLinkResolution: 'legacy'})
    ]
})
export class AppRoutingModule {
}
