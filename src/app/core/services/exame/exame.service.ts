import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Subject} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ExameService {

    constructor(private http: HttpClient) {
    }

    pesquisar(associado: string, prestador: string, conveniado: string, dataAtendimento: string, confirmado: string, tipo: string) {
        const token = localStorage.getItem(btoa('access_token'));
        let subject = new Subject<any>();

        const headers = new HttpHeaders({
            'Authorization': 'Bearer ' + token,
            'x-associado': associado ? associado : '',
            'x-status': confirmado ? confirmado : '',
            'x-prestador': prestador ? prestador : '',
            'x-conveniado': conveniado ? conveniado : '',
            'x-data': dataAtendimento ? dataAtendimento : '',
            'x-tipo': tipo ? tipo : ''
        });

        this.http.get(this.getBaseUrl().concat('exames/pesquisar'),
            {headers: headers}).subscribe(res => {
            subject.next(res);
        }, err => {
            if (err.status === 401) {
                subject.next(err);
            }
            if (err.status === 403) {
                subject.next(err);
            }
            if (err.status === 404) {
                subject.next(err);
            }
        });
        return subject.asObservable();
    }

    aprovarExame(status: string, id: number) {
        const token = localStorage.getItem(btoa('access_token'));
        let subject = new Subject<any>();

        const headers = new HttpHeaders({
            'Authorization': 'Bearer ' + token,
            'x-status': status
        });

        this.http.put(this.getBaseUrl().concat('exames/' + id), null,
            {headers: headers, observe: 'response'}).subscribe(res => {
            subject.next(res);
        }, err => {
            subject.next(err);
        });

        return subject.asObservable();
    }

    obterExames(id: string) {
        const token = localStorage.getItem(btoa('access_token'));
        const headers = new HttpHeaders({
            'Authorization': 'Bearer ' + token
        });
        return this.http.get(this.getBaseUrl().concat('atendimentos/' + id), {
            headers: headers
        });
    }

    protected getBaseUrl(): string {
        return environment.base_url_atendimento;
    }
}
