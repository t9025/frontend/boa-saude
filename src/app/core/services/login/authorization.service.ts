import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthorizationService {

    constructor(private http: HttpClient,
                private router: Router
    ) {
    }

    login(username: string, password: string) {
        const formData = new FormData();
        formData.append('scope', 'web')
        formData.append('grant_type', 'password')
        formData.append('username', username)
        formData.append('password', password)

        let subject = new Subject<any>();
        const headers = new HttpHeaders({'Authorization': 'Basic ' + btoa('admin' + ':' + '1209873')});

        this.http.post(this.getBaseUrlAuth().concat('oauth/token'), formData,
            {headers: headers, observe: 'response'}).subscribe(res => {
            subject.next(res);
        }, err => {
            subject.next(err);
        });
        return subject.asObservable();
    }

    protected getBaseUrlAuth(): string {
        return environment.base_url_auth;
    }

    logout() {
        localStorage.clear();
        return this.router.navigateByUrl('/login');
    }

    isLoggedIn() {
        const token = localStorage.getItem(btoa('access_token'));
        return !!token
    }
}
