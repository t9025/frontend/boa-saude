import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Subject} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {AtendimentoDTO} from '../../../views/pages/atendimento/resource/atendimento-dto';
import {AtualizarAtendimentoDTO} from '../../../views/pages/atendimento/resource/atualizar-atendimento-dto';

@Injectable({
    providedIn: 'root'
})
export class AtendimentoService {

    constructor(private http: HttpClient) {
    }

    pesquisar(associado: string, prestador: string, conveniado: string, dataAtendimento: string, confirmado: string) {
        const token = localStorage.getItem(btoa('access_token'));
        let subject = new Subject<any>();

        const headers = new HttpHeaders({
            'Authorization': 'Bearer ' + token,
            'x-associado': associado ? associado : '',
            'x-status': confirmado ? confirmado : '',
            'x-prestador': prestador ? prestador : '',
            'x-conveniado': conveniado ? conveniado : '',
            'x-data': dataAtendimento ? dataAtendimento : ''
        });

        this.http.get(this.getBaseUrlAtendimento().concat('atendimentos/pesquisar'),
            {headers: headers}).subscribe(res => {
            subject.next(res);
        }, err => {
            if (err.status === 401) {
                subject.next(err);
            }
            if (err.status === 403) {
                subject.next(err);
            }
            if (err.status === 404) {
                subject.next(err);
            }
        });
        return subject.asObservable();
    }

    protected getBaseUrlAtendimento(): string {
        return environment.base_url_atendimento;
    }

    salvar(atendimentoDTO: AtendimentoDTO) {
        const token = localStorage.getItem(btoa('access_token'));
        let subject = new Subject<any>();

        const headers = new HttpHeaders({
            'Authorization': 'Bearer ' + token
        });

        this.http.post(this.getBaseUrlAtendimento().concat('atendimentos'), atendimentoDTO,
            {headers: headers, observe: 'response'}).subscribe(res => {
            subject.next(res);
        }, err => {
            subject.next(err);
        });
        return subject.asObservable();
    }

    atualizarAtendimento(atualizarAtendimentoDTO: AtualizarAtendimentoDTO, id: number) {
        const token = localStorage.getItem(btoa('access_token'));
        let subject = new Subject<any>();

        const headers = new HttpHeaders({
            'Authorization': 'Bearer ' + token
        });

        this.http.put(this.getBaseUrlAtendimento().concat('atendimentos/' + id), atualizarAtendimentoDTO,
            {headers: headers, observe: 'response'}).subscribe(res => {
            subject.next(res);
        }, err => {
            subject.next(err);
        });

        return subject.asObservable();
    }

    obterAtendimento(id: string) {
        const token = localStorage.getItem(btoa('access_token'));
        const headers = new HttpHeaders({
            'Authorization': 'Bearer ' + token
        });
        return this.http.get(this.getBaseUrlAtendimento().concat('atendimentos/' + id), {
            headers: headers
        });
    }
}
