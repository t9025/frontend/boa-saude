import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthorizationService} from '../login/authorization.service';


@Injectable({
	providedIn: 'root'
})
export class AuthGuardChild implements CanActivateChild {

	constructor(private router: Router,
				private authorizationService: AuthorizationService) {
	}


	canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		if (this.authorizationService.isLoggedIn()) {
			return true;
		} else {
			console.log('Unauthorized: Full authentication is required to access this resource')
			localStorage.clear();
			return this.router.navigate(['/login']);
		}
	}
}
