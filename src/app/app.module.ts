import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {registerLocaleData} from '@angular/common';
import localePt from '@angular/common/locales/pt';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {ThemeModule} from './views/theme/theme.module';
import {ToastrModule} from 'ngx-toastr';
import {ConfirmationModalComponent} from './views/partials/confirmation-modal/confirmation-modal.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';

registerLocaleData(localePt);

@NgModule({
    declarations: [
        AppComponent,
        ConfirmationModalComponent,
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        RouterModule,
        ThemeModule,
        ToastrModule.forRoot({
            positionClass: 'toast-bottom-right',
            progressAnimation: 'decreasing',
            preventDuplicates: true,
            progressBar: true,
        }),
        MatDialogModule,
        MatCardModule,
        MatButtonModule
    ],
    exports: [RouterModule],
    providers: [
        {provide: LOCALE_ID, useValue: 'pt-BR'},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
