module.exports = [
    {
        context: [
            '/auth/token'
        ],
        target: 'http://localhost:8080/api/bs-authentication-service/v1/',
        pathRewrite: {'^/api': ''}
    }
];
